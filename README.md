# Gitlab

An ansible role to install and configure gitlab omnibus community edition on Debian 9.

## Role Variables

Please refer to the [defaults file](/defaults/main.yml) for an list of input parameters.   
See [playbook test file](/tests/playbook-gitlab.yml) for additional parameter (group creation, etc...).

The first ansible run create a personal admin token. This token is displayed at the end of the first run.   
You must create the variable named `gitlab_admin_token` with the contents of the token before starting the second ansible run.

Example:

```
gitlab_admin_token: 'bdP4MXfBA57WQUFBsgJE'
```
It is strongly recommended to protect this varibale with ansible vault.

## Testing

You can use [playbook test file](/tests/playbook-gitlab.yml) to test this role in a test environment.

## License

See the [licence](/LICENCE.txt) file.
